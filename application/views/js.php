<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?php echo site_url("assets/css/favicon.ico"); ?>">

<title>Helper js</title>

<!-- Bootstrap core CSS -->
<link href="<?php echo site_url("assets/css/bootstrap.min.css"); ?>" rel="stylesheet">
<link href="<?php echo site_url("assets/css/jquery-ui-1.11.4/jquery-ui.min.css"); ?>" rel="stylesheet">

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<link href="<?php echo site_url("assets/css/layout/ie10-viewport-bug-workaround.css"); ?>" rel="stylesheet">

<link href="<?php echo site_url("assets/css/bootstrap-select.min.css"); ?>" rel="stylesheet">
<link href="<?php echo site_url("assets/css/simplePagination.css"); ?>" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="<?php echo site_url("assets/css/obc.css"); ?>" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="<?php echo site_url("assets/js/layout/ie-emulation-modes-warning.js"); ?>"></script>


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body>



	<!-- Begin page content -->
	<div class="container-fluid">

		<div class="row">
			<form id="formNovo" class="form-horizontal" action="" method="post">

				<fieldset>

					<div class="form-group">
						<label for="idInputText" class="col-md-2 control-label">
							Input text
							<span class="text-danger">*</span>
						</label>
						<div class="col-md-4">
							<input value="valueInputText" type="text" id="idInputText" name="nameInputText" class="form-control" placeholder="" required autofocus>
						</div>
					</div>

					<div class="form-group">

						<label for="idSelect" class="col-md-2 control-label">
							Input select
							<span class="text-danger">*</span>
						</label>
						<div class="col-md-4">

							<select id="idSelect" name="nameSelect" class="selectpicker form-control">
								<option value="">Selecione</option>
								<option value="1" selected="selected">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>

						</div>
					</div>

					<div class="form-group">
						<label for="idSelectMultiple" class="col-md-2 control-label">
							Input select multiple
							<span class="text-danger">*</span>
						</label>
						<div class="col-md-10">
							<select id="idSelectMultiple" name="nameSelectMultiple" class="selectpicker form-control" title="" multiple>
								<option value="descricao" selected="selected">Descrição</option>
								<option value="dia_trabalho" selected="selected">Dias de trabalho</option>
								<option value="dia_folga" selected="selected">Dias de folga</option>
								<option value="ativo" selected="selected">Ativo</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">
							Input radio vertical
							<span class="text-danger">*</span>
						</label>
						<div class="col-md-10">
							<div class="radio">
								<label>
									<input type="radio" name="nameRadioVertical" id="idRadioVerticalOption1" value="valueRadioVerticalOption1" title="Option one" checked="checked">
									Option one
								</label>
							</div>
							<div class="radio">
								<label>
									<input type="radio" name="nameRadioVertical" id="idRadioVerticalOption2" value="valueRadioVerticalOption2" title="Option two">
									Option two
								</label>
							</div>
						</div>
					</div>


					<div class="form-group">
						<label class="col-md-2 control-label">
							Input radio horizontal
							<span class="text-danger">*</span>
						</label>
						<div class="col-md-10">
							<label class="radio-inline">
								<input type="radio" name="nameRadioHorizontal" id="idRadioHorizontalOption1" value="valueRadioHorizontalOption1" title="Option one"  checked="checked">
								Option one
							</label>
							<label class="radio-inline">
								<input type="radio" name="nameRadioHorizontal" id="idRadioHorizontalOption2" value="valueRadioHorizontalOption2" title="Option two">
								Option two
							</label>
						</div>

					</div>


					<div class="form-group">
						<label class="col-md-2 control-label">
							Input checkbox vertical
							<span class="text-danger">*</span>
						</label>
						<div class="col-md-10">
							<div class="checkbox">
								<label>
									<input id="idCheckboxVerticalOption1" name="nameCheckboxVertical" type="checkbox" title="Option one" value="valueCheckboxVerticalOption1">
									Option one
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input id="idCheckboxVerticalOption2" name="nameCheckboxVertical" type="checkbox" title="Option two" value="valueCheckboxVerticalOption2" checked="checked">
									Option two
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input id="idCheckboxVerticalOption3" name="nameCheckboxVertical" type="checkbox" title="Option three" value="valueCheckboxVerticalOption3">
									Option three
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input id="idCheckboxVerticalOption4" name="nameCheckboxVertical" type="checkbox" title="Option four" value="valueCheckboxVerticalOption4">
									Option four
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input id="idCheckboxVerticalOption5" name="nameCheckboxVertical" type="checkbox" title="Option five" value="valueCheckboxVerticalOption5">
									Option five
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-2 control-label">
							Input checkbox horizontal
							<span class="text-danger">*</span>
						</label>
						<div class="col-md-10">
							<label class="checkbox-inline">
								<input id="idCheckboxHorizontalOption1" name="nameCheckboxHorizontal" type="checkbox" title="Option one" value="valueCheckboxHorizontalOption1"  checked="checked">
									Option one
							</label>
							<label class="checkbox-inline">
								<input id="idCheckboxHorizontalOption2" name="nameCheckboxHorizontal" type="checkbox" title="Option two" value="valueCheckboxHorizontalOption2">
									Option two
							</label>
							<label class="checkbox-inline">
								<input id="idCheckboxHorizontalOption3" name="nameCheckboxHorizontal" type="checkbox" title="Option three" value="valueCheckboxHorizontalOption3">
									Option three
							</label>
							<label class="checkbox-inline">
								<input id="idCheckboxHorizontalOption4" name="nameCheckboxHorizontal" type="checkbox" title="Option four" value="valueCheckboxHorizontalOption4">
									Option four
							</label>
							<label class="checkbox-inline">
								<input id="idCheckboxHorizontalOption5" name="nameCheckboxHorizontal" type="checkbox" title="Option five" value="valueCheckboxHorizontalOption5">
									Option five
							</label>

						</div>
					</div>

					<div class="form-group">
						<label for="idTextarea" class="col-md-2 control-label">
							Input text area
							<span class="text-danger">*</span>
						</label>
						<div class="col-md-10">
							<textarea id="idTextarea" class="form-control" rows="3"></textarea>
						</div>
					</div>

					<div class="form-group">
						<label for="inputNovoDiaTrabalho" class="col-md-2 control-label">
							InputDesc
							<span class="text-danger">*</span>
						</label>
						<div class="col-md-10"></div>
					</div>

				</fieldset>

				<button class="btn btn-primary" type="button" id="setValue">Set value</button>
			</form>
		</div>

	</div>



	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo site_url("assets/js/jquery-2.2.0.min.js"); ?>"></script>
	<script src="<?php echo site_url("assets/css/jquery-ui-1.11.4/jquery-ui.min.js"); ?>"></script>
	<script src="<?php echo site_url("assets/js/bootstrap.min.js"); ?>"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="<?php echo site_url("assets/js/layout/ie10-viewport-bug-workaround.js"); ?>"></script>
	<!-- Validate jquery js -->
	<script src="<?php echo site_url("assets/js/jquery.validate.js"); ?>"></script>
	<script src="<?php echo site_url("assets/js/jquery.maskedinput.js"); ?>"></script>
	<script src="<?php echo site_url("assets/js/bootstrap-select.min.js"); ?>"></script>
	<script src="<?php echo site_url("assets/js/jquery.simplePagination.js"); ?>"></script>

	<script src="<?php echo site_url("assets/js/script.js"); ?>"></script>

</body>
</html>