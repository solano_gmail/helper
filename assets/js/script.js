$("document").ready(function() {

	$(document).on("click","#setValue", function(){
		setInputText($("#idInputText"), "Ok");
//		setSelectpikerByDesc($("#idSelect"), "2");
//		setSelectpikerMultipleByDescs($("#idSelectMultiple"), ["Descrição","Ativo"]);
//		setSelectpikerByValue($("#idSelect"), "3");
//		setSelectpikermultipleByValues($("#idSelectMultiple"), ["descricao","ativo", "dia_trabalho"]);
//		setRadioByDesc($("#formNovo"), "nameRadioHorizontal", "Option two" );
//		setRadioByDesc($("#formNovo"), "nameRadioVertical", "Option two" );
//		setRadioByValue($("#formNovo"), "nameRadioHorizontal", "valueRadioHorizontalOption2" );
//		setRadioByValue($("#formNovo"), "nameRadioVertical", "valueRadioVerticalOption2" );
//		setCheckboxByValues($("#formNovo"), "nameCheckboxVertical", ["valueCheckboxVerticalOption1","valueCheckboxVerticalOption3","valueCheckboxVerticalOption5"] );
//		setCheckboxByValues($("#formNovo"), "nameCheckboxHorizontal", ["valueCheckboxHorizontalOption2","valueCheckboxHorizontalOption3","valueCheckboxHorizontalOption4","xx"] );
		
//		setCheckboxByTitles($("#formNovo"), "nameCheckboxVertical", ["Option one","Option four","Option three","xx"] );
//		setCheckboxByTitles($("#formNovo"), "nameCheckboxHorizontal", ["Option one","Option five", "Option two","xx"] );
		
//		setTextarea($("#idTextarea"), "text area texto");
		
	})

});

function setTextarea($el, text){
	$el.val(text);
}

function setCheckboxByTitles($container, name, optionTitles){
	$radios = $container.find("input[name*='"+name+"']");
	
	$radios.each(function(){
		$option = $(this);
		$(this).prop("checked", false);
		$.each(optionTitles, function(index, value){
			if($option.attr('title') == value){
				$option.prop("checked", true);
				return;
			}
		})
	});
}

function setCheckboxByValues($container, name, optionValues){
	$radios = $container.find("input[name*='"+name+"']");
	
	$radios.each(function(){
		$option = $(this);
		$(this).prop("checked", false);
		$.each(optionValues, function(index, value){
			console.log($option.val()+" == "+value);
			if($option.val() == value){
				$option.prop("checked", true);
				return;
			}
		})
	});
}

function setRadioByValue($container, name, optionValue){
	$radios = $container.find("input[name*='"+name+"']");
	
	$radios.each(function(){
		if( $(this).val() == optionValue ){
			$(this).prop("checked", true);
		}else{
			$(this).prop("checked", false);
		}
	});
}

function setRadioByDesc($container, name, optionDesc){
	$radios = $container.find("input[name*='"+name+"']");
	
	$radios.each(function(){
		if( $(this).attr('title') == optionDesc ){
			$(this).prop("checked", true);
		}else{
			$(this).prop("checked", false);
		}
	});
}

function setInputText($el, value){
	$el.val(value);
}

function setSelectpikerByDesc($el, optionDesc){
	$options = $el.find("option");
	$el.selectpicker('deselectAll');
	
	$options.each(function(){
		if($(this).text() == optionDesc){
			$el.selectpicker('val', $(this).val());
			return;
		}
	});
}

function setSelectpikerByValue($el, optionValue){
	$options = $el.find("option");
	$el.selectpicker('deselectAll');
	
	$options.each(function(){
		if($(this).val() == optionValue){
			$el.selectpicker('val', optionValue);
			return;
		}
	});
}

function setSelectpikerMultipleByDescs($el, optionDescArray){
	$options = $el.find("option");
	$el.selectpicker('deselectAll');
	arrayValues = [];
	$options.each(function(){
		var $option = $(this);
		$.each(optionDescArray, function(index, val){
			if($option.text() == val){
				arrayValues.push($option.val());
				
			}
		});
	});
	$el.selectpicker('val',arrayValues);
}

function setSelectpikermultipleByValues($el, optionValuesArray){
	$options = $el.find("option");
	$el.selectpicker('deselectAll');
	$el.selectpicker('val',optionValuesArray);
}